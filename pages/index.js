import React, { Component } from 'react'

import {ig, loginLink} from '../ig'

export default class IndexPage extends Component {
  state = {
    access_token: false,
    user: false,
    media: false
  }

  componentDidMount () {
    let access_token = window.location.hash.replace('#access_token=', '')
    if (access_token){
      localStorage.access_token = access_token
      // hide the code by changing URL to 2-ago
      history.go(-2)
    } else {
      access_token = localStorage.access_token
    }

    if (access_token){
      this.setState({access_token})
      ig('users/self')
        .then(user => this.setState({user}))
      ig('users/self/media/recent')
        .then(media => this.setState({media}))
    }
  }

  render () {
    const { access_token, user, media } = this.state
    return (
      <div>
        {!access_token && (
          <a href={loginLink}>login</a>
        )}
        {user && (
          <div className="user">
            <a href={`https://www.instagram.com/${user.data.username}`} className="person">
              <img src={user.data.profile_picture} />
            </a>
            <div className="counts">
              Following: {user.data.counts.follows} &bull; Followed By: {user.data.counts.followed_by}
            </div>
            <div className="bio">
              {user.data.bio}
            </div>
            <hr/>
          </div>
        )}
        {media && media.data.map(m => (
          <div key={m.id} className="mediaItem">
            <div className="mediaObject">
              {m.videos && <video src={m.videos.standard_resolution.url} controls />}
              {!m.videos && <img src={m.images.standard_resolution.url} />}
            </div>
            {m.caption && (
              <div className="caption">
                <a href={`https://www.instagram.com/${m.caption.from.username}`} className="person">
                  <img src={m.caption.from.profile_picture} />
                </a>
                <div className="text">
                  {m.caption.text}
                </div>
              </div>
            )}
            <div className="likes">{m.likes.count}</div>
            <hr/>
          </div>

        ))}
        <style jsx>{`
          * {
            font-family: sans-serif;
          }
          .caption {
            display: flex;
            align-items: center;
          }
          .caption img {
            width: 30px;
          }
          .likes {
            position:relative;
          }
          .likes:before {
            color: red;
            content: '❤';
            font-size: 24px;
          }
        `}</style>
      </div>
    )
  }
}
