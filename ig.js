const CLIENT_ID = 'c78a4dea0a644d74964cb092155164cb'

export const loginLink = `https://api.instagram.com/oauth/authorize/?client_id=${CLIENT_ID}&redirect_uri=${encodeURIComponent('http://localhost:3000')}&response_type=token`

// get some data from instagram
export const ig = url => fetch(`https://api.instagram.com/v1/${url}?access_token=${localStorage.access_token}`)
  .then(r => r.json())

export default ig
